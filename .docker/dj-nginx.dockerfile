FROM nginx
# COPY ./nginx /etc/nginx/
COPY ./server /opt/
RUN apt update
RUN apt install python3 pip -y 
RUN pip install -r /opt/requirements.txt