from django.contrib import admin

from host import models

admin.site.register(models.GroupLocation)
admin.site.register(models.CustomLocation)
admin.site.register(models.Vps)
admin.site.register(models.Domain)
admin.site.register(models.Ssl)
