import os
import subprocess
from datetime import datetime, time
from pprint import pprint

from django.db import models
from django.db.models.signals import pre_save

# from host.signals import create_config_nginx


class GroupLocation(models.Model):
    name = models.CharField(max_length=50)


class Domain(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Ssl(models.Model):
    domain = models.ForeignKey(Domain, on_delete=models.CASCADE, editable=False)
    create_at = models.DateTimeField(editable=False)

    def __str__(self):
        return f'{self.create_at} {self.domain.name}'


class CustomLocation(models.Model):
    location = models.CharField(max_length=50)
    direction = models.CharField(max_length=50)
    port = models.IntegerField()
    group_location = models.ManyToManyField(GroupLocation, blank=True)


class Vps(models.Model):
    domain = models.ForeignKey(Domain, on_delete=models.CASCADE)
    direction = models.CharField(max_length=255, blank=True)
    port = models.IntegerField()
    ssl = models.BooleanField(default=False)
    locations = models.ManyToManyField(CustomLocation, blank=True)
    group_location = models.ManyToManyField(GroupLocation, blank=True)

    def __str__(self):
        protocol = 'http'
        if self.ssl:
            protocol = 'https'
        return f'{protocol}://{self.domain.name} - {self.direction}:{self.port}'


def create_ssl(instance: Ssl, **kwargs):
    work_space = '/etc/nginx/ssl/'
    file_key = f'{work_space}{instance.domain.name}'
    subject = f'"/C=US/ST=Mars/L=iTranswarp/O=iTranswarp/OU=iTranswarp/CN={instance.domain.name}"'
    commands = [f'openssl genrsa -out {file_key}.key 4096',
                f'openssl req -new -x509 -text -subj {subject} -key {file_key}.key -out {file_key}.crt']
    for command in commands:
        os.system(command)
    # file_key = f'{work_space}{instance.domain.name}'
    # subject = f'/C=US/ST=Mars/L=iTranswarp/O=iTranswarp/OU=iTranswarp/CN={instance.domain.name}'
    # create_key = f'openssl genrsa -out {file_key}.key 1024'
    # create_ssl = f'openssl req -x509 -nodes -days 365 -newkey rsa:2048 -subj {subject} -keyout {file_key}.key -out {file_key}.csr'
    # live_certs = f'openssl x509 -req -days 3650 -in {file_key}.csr -signkey {file_key}.key -out {file_key}.crt'
    # create_ssl = f'openssl req -subj "/CN={instance.domain.name}/O=Let\'s Encrypt" -x509 -newkey rsa:4096 -nodes -keyout {file_key}-key.pem -out {file_key}-cert.pem -days 365'
    # subprocess.Popen(create_key.split(' '), stdin=subprocess.PIPE, stdout=subprocess.PIPE).wait()
    # subprocess.Popen(create_ssl.split(' ')).wait()
    # subprocess.Popen(live_certs.split(' ')).wait()


def create_config_nginx(instance: Vps, **kwargs):
    if instance.ssl:
        Ssl.objects.create(domain=Domain.objects.filter(name=instance.domain.name)[0], create_at=datetime.now())


pre_save.connect(create_config_nginx, sender=Vps)
pre_save.connect(create_ssl, sender=Ssl)
